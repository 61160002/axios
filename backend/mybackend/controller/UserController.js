const userController = {
  userList: [
    { id: 1, name: 'Erng', gender: 'F' },
    { id: 2, name: 'Milo', gender: 'M' }
  ],
  lastId: 3,
  addUser(user) {
    user.id = this.lastId++
    this.userList.push(user)
    return user
  },
  updateUser(user) {
    // หา index ใน userList โดยหาแบบ ไปทีละ item โดยมีเงือนไข item.id === user.id
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser(id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
    return { id }
  },
  getUsers() {
    return [...this.userList]
  },
  getUser(id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}

module.exports = userController
