const userService = {
  userList: [
    { id: 1, name: 'Erng', gender: 'F' },
    { id: 2, name: 'Milo', gender: 'M' }
  ],
  lastId: 3,
  addUser(user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser(user) {
    // หา index ใน userList โดยหาแบบ ไปทีละ item โดยมีเงือนไข item.id === user.id
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser(user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers() {
    return [...this.userList]
  }
}

export default userService
